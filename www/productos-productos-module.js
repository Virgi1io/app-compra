(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["productos-productos-module"],{

/***/ "./src/app/productos/productos.module.ts":
/*!***********************************************!*\
  !*** ./src/app/productos/productos.module.ts ***!
  \***********************************************/
/*! exports provided: ProductosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductosPageModule", function() { return ProductosPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _productos_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./productos.page */ "./src/app/productos/productos.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _productos_page__WEBPACK_IMPORTED_MODULE_5__["ProductosPage"]
    }
];
var ProductosPageModule = /** @class */ (function () {
    function ProductosPageModule() {
    }
    ProductosPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_productos_page__WEBPACK_IMPORTED_MODULE_5__["ProductosPage"]]
        })
    ], ProductosPageModule);
    return ProductosPageModule;
}());



/***/ }),

/***/ "./src/app/productos/productos.page.html":
/*!***********************************************!*\
  !*** ./src/app/productos/productos.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Productos</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button autoHide=\"true\"></ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content color=secondary scrollable=\"true\">\n\n  <ion-searchbar\n      [animated]=\"true\"\n      (ionInput)=\"filtrar($event)\"\n      (ionCancel)=\"iniciar_lista()\">\n    </ion-searchbar>\n\n    <ion-list>\n      <ion-item-sliding *ngFor=\"let producto of lista_productos\" #slidingItem>\n        <ion-item (press)=\"activar_seleccion_multiple(producto)\" (click)=\"pasar_a_pendiente(producto)\">\n          <ion-checkbox [(ngModel)]=\"producto.seleccionado\" style=\"margin-right: 10px;\" [hidden]=\"!seleccion_multiple\"></ion-checkbox>\n          <p><b>Nombre: </b>{{producto.nombre}}<br>\n          <b>Fecha de registro: </b>{{producto.fecha_alta}}<br>\n          <b>Codigo de barras: </b>{{producto.barcode!==''?'Si':'No'}}</p>\n        </ion-item>\n        <ion-item-options side=\"end\">\n          <ion-item-option color=\"danger\" (click)=\"eliminar_registrado(producto, slidingItem)\">Borrar</ion-item-option>\n        </ion-item-options>\n      </ion-item-sliding>\n    </ion-list> \n\n</ion-content>\n\n<ion-footer>\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" [hidden]=\"seleccion_multiple\">\n\n    <ion-fab-button >\n      <ion-icon name=\"add\"></ion-icon>\n    </ion-fab-button>\n\n    <ion-fab-list side=\"top\">\n      <ion-fab-button (click)=\"nuevo()\">\n        <ion-icon name=\"add\"></ion-icon>\n      </ion-fab-button>\n      \n      <ion-fab-button (click)=\"nuevo_con_codigo()\">\n        <ion-icon name=\"barcode\"></ion-icon>\n      </ion-fab-button>\n\n    </ion-fab-list>\n  </ion-fab>\n\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" [hidden]=\"!seleccion_multiple\">\n    <ion-fab-button (click)=\"pasar_compra()\">\n      <ion-icon name=\"add\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/productos/productos.page.scss":
/*!***********************************************!*\
  !*** ./src/app/productos/productos.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .list-md {\n  padding: 0px 0px 0px 0px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ZpcmdpbGlvL0RvY3VtZW50b3MvUHJveWVjdG9zL0NvbXByYS9zcmMvYXBwL3Byb2R1Y3Rvcy9wcm9kdWN0b3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRVEsd0JBQXdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0b3MvcHJvZHVjdG9zLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0eyAgICBcbiAgICAubGlzdC1tZHtcbiAgICAgICAgcGFkZGluZzogMHB4IDBweCAwcHggMHB4O1xuICAgIH1cblxuICAgIC5jaGVjay1ib3h7XG4gICAgICAgIFxuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/productos/productos.page.ts":
/*!*********************************************!*\
  !*** ./src/app/productos/productos.page.ts ***!
  \*********************************************/
/*! exports provided: ProductosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductosPage", function() { return ProductosPage; });
/* harmony import */ var _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/barcode-scanner/ngx */ "./node_modules/@ionic-native/barcode-scanner/ngx/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ProductosPage = /** @class */ (function () {
    function ProductosPage(router, platform, firestore, alertctrl, toastctrl, scanner) {
        var _this = this;
        this.router = router;
        this.platform = platform;
        this.firestore = firestore;
        this.alertctrl = alertctrl;
        this.toastctrl = toastctrl;
        this.scanner = scanner;
        this.lista_productos = [];
        this.seleccion_multiple = false;
        this.platform.backButton.subscribe(function () {
            _this.router.navigateByUrl('/productos');
            _this.seleccion_multiple = false;
            _this.lista_productos.forEach(function (producto) {
                producto.seleccionado = false;
            });
        });
    }
    ProductosPage.prototype.ngOnInit = function () {
        this.iniciar_lista();
    };
    ProductosPage.prototype.iniciar_lista = function () {
        var _this = this;
        this.lista_productos = [];
        this.firestore.collection('producto', function (ref) { return ref.orderBy('nombre'); }).valueChanges().subscribe(function (data) {
            console.log(data);
            data.forEach(function (item) {
                item.seleccionado = false;
            });
            _this.lista_productos = data;
        });
    };
    ProductosPage.prototype.filtrar = function (event) {
        var value = event.target.value;
        if (value && value.trim() != '') {
            this.lista_productos = this.lista_productos.filter(function (item) {
                return (item.nombre.toLowerCase().indexOf(value.toLowerCase()) > -1);
            });
        }
        else {
            this.iniciar_lista();
        }
    };
    ProductosPage.prototype.nuevo = function (barcode) {
        if (barcode === void 0) { barcode = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var self, nuevo_producto, alert_registrar;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        self = this;
                        return [4 /*yield*/, this.alertctrl.create({
                                header: 'Añadir nuevo producto:',
                                subHeader: '¿Desea registrar el nuevo producto?',
                                message: 'Para registrarlo escriba un nombre y pulse ok, cancelar si no desea registrarlo.',
                                inputs: [{
                                        type: 'text',
                                        name: 'nombre',
                                        value: '',
                                        label: 'Nombre',
                                        placeholder: 'Nombre del nuevo producto',
                                    }],
                                buttons: [{
                                        text: 'Cancelar',
                                        role: 'cancel',
                                        handler: function () {
                                        }
                                    }, {
                                        text: 'Aceptar',
                                        handler: function (res) {
                                            return __awaiter(this, void 0, void 0, function () {
                                                var toast_alert;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            // Creamos un nuevo producto.
                                                            nuevo_producto = {
                                                                nombre: res.nombre,
                                                                barcode: barcode,
                                                                fecha_alta: new Date().toLocaleString()
                                                            };
                                                            console.log(nuevo_producto);
                                                            if (!(nuevo_producto.nombre !== '')) return [3 /*break*/, 1];
                                                            self.firestore.collection('producto').add(nuevo_producto).then(function (add_res) {
                                                                console.log('Confirm Okay', add_res);
                                                            }).catch(function (err) { return console.error(err); });
                                                            return [3 /*break*/, 4];
                                                        case 1: return [4 /*yield*/, self.toastctrl.create({
                                                                message: 'El nombre del producto no puede estar vacio!',
                                                                position: 'middle',
                                                                duration: 3000,
                                                            })];
                                                        case 2:
                                                            toast_alert = _a.sent();
                                                            return [4 /*yield*/, toast_alert.present()];
                                                        case 3:
                                                            _a.sent();
                                                            _a.label = 4;
                                                        case 4: return [2 /*return*/];
                                                    }
                                                });
                                            });
                                        }
                                    },]
                            })];
                    case 1:
                        alert_registrar = _a.sent();
                        return [4 /*yield*/, alert_registrar.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductosPage.prototype.nuevo_con_codigo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self, barcode, producto_existente, toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        self = this;
                        return [4 /*yield*/, this.scanner.scan({ showTorchButton: true })];
                    case 1:
                        barcode = _a.sent();
                        console.log('Leido codigo', barcode.text);
                        return [4 /*yield*/, this.firestore.collection('producto', function (ref) { return ref.where('barcode', '==', barcode.text); }).get().toPromise()];
                    case 2:
                        producto_existente = _a.sent();
                        console.log('productos encontrados :', producto_existente.size);
                        if (!(producto_existente.size === 0)) return [3 /*break*/, 5];
                        // El producto no existe, vamos a registrarlo.
                        this.nuevo(barcode.text);
                        return [4 /*yield*/, self.toastctrl.create({
                                message: 'Producto registrado!',
                                position: 'middle',
                                duration: 3000
                            })];
                    case 3:
                        toast = _a.sent();
                        return [4 /*yield*/, toast.present()];
                    case 4:
                        _a.sent();
                        return [3 /*break*/, 6];
                    case 5:
                        if (producto_existente.size === 1) {
                            // El producto existe vamos a añadirlo a la lista de la compra pendiente.
                            producto_existente.forEach(function (item) {
                                return __awaiter(this, void 0, void 0, function () {
                                    return __generator(this, function (_a) {
                                        self.pasar_a_pendiente(item.data());
                                        return [2 /*return*/];
                                    });
                                });
                            });
                        }
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    ProductosPage.prototype.eliminar_registrado = function (producto, slidingItem) {
        return __awaiter(this, void 0, void 0, function () {
            var self, alert_registrar;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(producto);
                        self = this;
                        return [4 /*yield*/, this.alertctrl.create({
                                header: 'Eliminar el producto ' + producto.nombre,
                                subHeader: '¿Desea eliminar el producto?',
                                message: 'Para eliminarlo pulse aceptar, cancelar si no desea registrarlo.',
                                buttons: [{
                                        text: 'Cancelar',
                                        role: 'cancel',
                                        handler: function () {
                                        }
                                    }, {
                                        text: 'Aceptar',
                                        handler: function () {
                                            return __awaiter(this, void 0, void 0, function () {
                                                var producto_a_borrar;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0: return [4 /*yield*/, self.firestore.collection('producto', function (ref) { return ref.where('nombre', '==', producto.nombre); }).get().toPromise()];
                                                        case 1:
                                                            producto_a_borrar = _a.sent();
                                                            producto_a_borrar.forEach(function (item) {
                                                                slidingItem.close();
                                                                item.ref.delete();
                                                            });
                                                            return [2 /*return*/];
                                                    }
                                                });
                                            });
                                        }
                                    },]
                            })];
                    case 1:
                        alert_registrar = _a.sent();
                        return [4 /*yield*/, alert_registrar.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductosPage.prototype.pasar_a_pendiente = function (producto, slidingItem, multiple) {
        if (slidingItem === void 0) { slidingItem = {}; }
        if (multiple === void 0) { multiple = false; }
        return __awaiter(this, void 0, void 0, function () {
            var self, alerta;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        self = this;
                        return [4 /*yield*/, this.alertctrl.create({
                                header: 'Unidades:',
                                subHeader: '¿Cuantas unidades del producto ' + producto.nombre + ' desea añadir a la cesta ?',
                                message: 'Seleccione con el teclado cuantas unidades del producto quiere añadir a la cesta.',
                                inputs: [{
                                        name: 'unidades',
                                        label: 'Unidades',
                                        type: 'number',
                                        value: 1,
                                        min: 1,
                                        max: 50
                                    },],
                                buttons: [{
                                        text: 'Aceptar',
                                        handler: function (res) {
                                            return __awaiter(this, void 0, void 0, function () {
                                                var nuevo_pendiente, existe_pendiente, toast;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            nuevo_pendiente = producto;
                                                            nuevo_pendiente.fecha_pendiente = new Date().toLocaleString();
                                                            nuevo_pendiente.unidades = res.unidades;
                                                            return [4 /*yield*/, self.firestore.collection('pendiente', function (ref) { return ref.where('nombre', '==', nuevo_pendiente.nombre); }).get().toPromise()];
                                                        case 1:
                                                            existe_pendiente = _a.sent();
                                                            console.log(existe_pendiente);
                                                            if (!(existe_pendiente.size === 0)) return [3 /*break*/, 5];
                                                            console.log('Existe nuevo pendiente');
                                                            return [4 /*yield*/, self.firestore.collection('pendiente').add(nuevo_pendiente)];
                                                        case 2:
                                                            _a.sent();
                                                            return [4 /*yield*/, self.toastctrl.create({
                                                                    message: 'Producto añadido a la cesta',
                                                                    position: 'middle',
                                                                    duration: 3000
                                                                })];
                                                        case 3:
                                                            toast = _a.sent();
                                                            return [4 /*yield*/, toast.present()];
                                                        case 4:
                                                            _a.sent();
                                                            _a.label = 5;
                                                        case 5: return [2 /*return*/];
                                                    }
                                                });
                                            });
                                        }
                                    },
                                    {
                                        text: 'Cancelar',
                                        handler: function () { },
                                    },],
                            })];
                    case 1:
                        alerta = _a.sent();
                        if (!((!this.seleccion_multiple) || (this.seleccion_multiple && multiple))) return [3 /*break*/, 3];
                        return [4 /*yield*/, alerta.present()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductosPage.prototype.activar_seleccion_multiple = function (producto) {
        this.seleccion_multiple = true;
        producto.seleccionado = true;
    };
    ProductosPage.prototype.pasar_compra = function () {
        var _this = this;
        console.log("seleccion multiple", this.lista_productos);
        this.lista_productos.forEach(function (producto) {
            if (producto.seleccionado) {
                _this.pasar_a_pendiente(producto, {}, true);
            }
        });
    };
    ProductosPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-productos',
            template: __webpack_require__(/*! ./productos.page.html */ "./src/app/productos/productos.page.html"),
            styles: [__webpack_require__(/*! ./productos.page.scss */ "./src/app/productos/productos.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_0__["BarcodeScanner"]])
    ], ProductosPage);
    return ProductosPage;
}());



/***/ })

}]);
//# sourceMappingURL=productos-productos-module.js.map