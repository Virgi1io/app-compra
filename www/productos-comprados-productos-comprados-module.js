(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["productos-comprados-productos-comprados-module"],{

/***/ "./src/app/productos-comprados/productos-comprados.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/productos-comprados/productos-comprados.module.ts ***!
  \*******************************************************************/
/*! exports provided: ProductosCompradosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductosCompradosPageModule", function() { return ProductosCompradosPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _productos_comprados_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./productos-comprados.page */ "./src/app/productos-comprados/productos-comprados.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _productos_comprados_page__WEBPACK_IMPORTED_MODULE_5__["ProductosCompradosPage"]
    }
];
var ProductosCompradosPageModule = /** @class */ (function () {
    function ProductosCompradosPageModule() {
    }
    ProductosCompradosPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_productos_comprados_page__WEBPACK_IMPORTED_MODULE_5__["ProductosCompradosPage"]]
        })
    ], ProductosCompradosPageModule);
    return ProductosCompradosPageModule;
}());



/***/ }),

/***/ "./src/app/productos-comprados/productos-comprados.page.html":
/*!*******************************************************************!*\
  !*** ./src/app/productos-comprados/productos-comprados.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Productos Comprados</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button autoHide=\"true\"></ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content color=\"secondary\">\n  <ion-searchbar\n    [animated]=\"true\"\n    (ionInput)=\"filtrar($event)\"\n    (ionCancel)=\"iniciar_lista()\">\n  </ion-searchbar>\n\n  <ion-list>\n    <ion-item *ngFor=\"let producto of lista_comprados\">\n        <p><b>Nombre: </b>{{producto.nombre}}<br>\n          <b>Fecha de compra: </b>{{producto.fecha_compra}}<br>\n          <b>Cantidad: </b>{{producto.unidades}}</p>      \n    </ion-item>\n  </ion-list>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/productos-comprados/productos-comprados.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/productos-comprados/productos-comprados.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .list-md {\n  padding: 0px 0px 0px 0px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ZpcmdpbGlvL0RvY3VtZW50b3MvUHJveWVjdG9zL0NvbXByYS9zcmMvYXBwL3Byb2R1Y3Rvcy1jb21wcmFkb3MvcHJvZHVjdG9zLWNvbXByYWRvcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFUSx3QkFBd0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3Rvcy1jb21wcmFkb3MvcHJvZHVjdG9zLWNvbXByYWRvcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdHsgICAgXG4gICAgLmxpc3QtbWR7XG4gICAgICAgIHBhZGRpbmc6IDBweCAwcHggMHB4IDBweDtcbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/productos-comprados/productos-comprados.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/productos-comprados/productos-comprados.page.ts ***!
  \*****************************************************************/
/*! exports provided: ProductosCompradosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductosCompradosPage", function() { return ProductosCompradosPage; });
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductosCompradosPage = /** @class */ (function () {
    function ProductosCompradosPage(firestore) {
        this.firestore = firestore;
        this.lista_comprados = [];
    }
    ProductosCompradosPage.prototype.ngOnInit = function () {
        this.iniciar_lista();
    };
    ProductosCompradosPage.prototype.iniciar_lista = function () {
        var _this = this;
        this.lista_comprados = [];
        this.firestore.collection('comprado').valueChanges().subscribe(function (data) {
            console.log(data);
            _this.lista_comprados = data;
        });
    };
    ProductosCompradosPage.prototype.filtrar = function (event) {
        var value = event.target.value;
        if (value && value.trim() != '') {
            this.lista_comprados = this.lista_comprados.filter(function (item) {
                return (item.nombre.toLowerCase().indexOf(value.toLowerCase()) > -1);
            });
        }
        else {
            this.iniciar_lista();
        }
    };
    ProductosCompradosPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-productos-comprados',
            template: __webpack_require__(/*! ./productos-comprados.page.html */ "./src/app/productos-comprados/productos-comprados.page.html"),
            styles: [__webpack_require__(/*! ./productos-comprados.page.scss */ "./src/app/productos-comprados/productos-comprados.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_0__["AngularFirestore"]])
    ], ProductosCompradosPage);
    return ProductosCompradosPage;
}());



/***/ })

}]);
//# sourceMappingURL=productos-comprados-productos-comprados-module.js.map