(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.html":
/*!*************************************!*\
  !*** ./src/app/home/home.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>\n      Mi Compra\n    </ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button autoHide=\"true\"></ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content color=\"secondary\">\n  <div class=\"cesta_llena\" *ngIf=\"!lista\">\n  \n    <ion-searchbar\n      [animated]=\"true\"\n      (ionInput)=\"filtrar($event)\"\n      (ionCancel)=\"iniciar_lista()\">\n    </ion-searchbar>\n\n    <ion-list color=\"secondary\">\n      <ion-item-sliding *ngFor=\"let producto of lista_productos_pendientes\" #slidingItem>\n        <ion-item>\n          <p><b>Nombre: </b>{{producto.nombre}}<br>\n          <b>Fecha pendiente: </b>{{producto.fecha_pendiente}}<br>\n          <b>Cantidad: </b>{{producto.unidades}}</p>\n        </ion-item>\n        <ion-item-options side=\"end\">\n          <ion-item-option (click)=\"producto_comprado(producto, slidingItem)\">Comprado</ion-item-option>\n          <ion-item-option color=\"danger\" (click)=\"producto_eliminado(producto, slidingItem)\">Eliminar</ion-item-option>\n        </ion-item-options>\n      </ion-item-sliding>\n\n    </ion-list>\n  </div>\n\n  <div class=\"cesta_vacía\" *ngIf=\"lista\">\n    <img src=\"../assets/img/relax.png\" class=\"logo-gabitel\">\n    <p>La compra esta hecha, pasa un buen día!</p>\n  </div>\n\n</ion-content>\n\n<ion-footer>\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button (click) = \"scan_barcode()\">\n      <ion-icon name=\"barcode\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-footer>"

/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@charset \"UTF-8\";\n:host .list-md {\n  padding: 0px 0px 0px 0px; }\n:host .cesta_vacía {\n  text-align: center; }\n:host .cesta_vacía p {\n    position: absolute;\n    top: 40%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    font-size: 140%;\n    text-shadow: 2px 2px #919191 f; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsIi9ob21lL3ZpcmdpbGlvL0RvY3VtZW50b3MvUHJveWVjdG9zL0NvbXByYS9zcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDQWhCO0VBR1Esd0JBQXdCLEVBQUE7QUFIaEM7RUFPUSxrQkFBa0IsRUFBQTtBQVAxQjtJQVVZLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsU0FBUztJQUNULHdDQUFnQztZQUFoQyxnQ0FBZ0M7SUFDaEMsZUFBZTtJQUNmLDhCQUF3QyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbjpob3N0IC5saXN0LW1kIHtcbiAgcGFkZGluZzogMHB4IDBweCAwcHggMHB4OyB9XG5cbjpob3N0IC5jZXN0YV92YWPDrWEge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cbiAgOmhvc3QgLmNlc3RhX3ZhY8OtYSBwIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA0MCU7XG4gICAgbGVmdDogNTAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICAgIGZvbnQtc2l6ZTogMTQwJTtcbiAgICB0ZXh0LXNoYWRvdzogMnB4IDJweCAjOTE5MTkxIGY7IH1cbiIsIjpob3N0eyAgICBcbiAgICBcbiAgICAubGlzdC1tZHtcbiAgICAgICAgcGFkZGluZzogMHB4IDBweCAwcHggMHB4O1xuICAgIH1cblxuICAgIC5jZXN0YV92YWPDrWF7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAgICAgICBwe1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgdG9wOiA0MCU7XG4gICAgICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTQwJTtcbiAgICAgICAgICAgIHRleHQtc2hhZG93OiAycHggMnB4IHJnYigxNDUsIDE0NSwgMTQ1KWY7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbiAgICBcblxuIl19 */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/local-notifications/ngx */ "./node_modules/@ionic-native/local-notifications/ngx/index.js");
/* harmony import */ var _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/barcode-scanner/ngx */ "./node_modules/@ionic-native/barcode-scanner/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var config = {
    desiredAccuracy: 10,
    stationaryRadius: 20,
    distanceFilter: 30,
    debug: true,
    stopOnTerminate: false,
};
var HomePage = /** @class */ (function () {
    function HomePage(scanner, notifyer, firestore, gps, alert, toast) {
        this.scanner = scanner;
        this.notifyer = notifyer;
        this.firestore = firestore;
        this.gps = gps;
        this.alert = alert;
        this.toast = toast;
        this.lista_productos_pendientes = [];
        this.lista = false;
    }
    /**
     * Metodo inicial de la lista.
     */
    HomePage.prototype.ngOnInit = function () {
        this.iniciar_lista();
    };
    /**
     * Inicia la lista de las compras pendientes
     */
    HomePage.prototype.iniciar_lista = function () {
        var _this = this;
        this.lista_productos_pendientes = [];
        this.firestore.collection('pendiente').valueChanges().subscribe(function (res) {
            _this.lista = res.length === 0;
            _this.lista_productos_pendientes = res;
        });
    };
    HomePage.prototype.filtrar = function (event) {
        var value = event.target.value;
        if (value && value.trim() != '') {
            this.lista_productos_pendientes = this.lista_productos_pendientes.filter(function (item) {
                return (item.nombre.toLowerCase().indexOf(value.toLowerCase()) > -1);
            });
        }
        else {
            this.iniciar_lista();
        }
    };
    /**
     * Escanea un código con el siguiente comportamiento:
     * Si el producto no esta registrado y no esta en la lista de la compra, lo registra y lo añade a la compra.
     * Si el producto no esta en la lista pero esta registrado, se añade a la lista de la compra.
     * Si el producto esta registrado y en la lista de la compra pasa a comprados y se elimina de la lista de la compra.
     */
    HomePage.prototype.scan_barcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self, barcode, producto_pendiente, producto_existente, alert_registrar;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        self = this;
                        return [4 /*yield*/, this.scanner.scan({ showTorchButton: true })];
                    case 1:
                        barcode = _a.sent();
                        return [4 /*yield*/, this.firestore.collection('pendiente', function (ref) { return ref.where('barcode', '==', barcode.text); }).get().toPromise()];
                    case 2:
                        producto_pendiente = _a.sent();
                        return [4 /*yield*/, this.firestore.collection('producto', function (ref) { return ref.where('barcode', '==', barcode.text); }).get().toPromise()];
                    case 3:
                        producto_existente = _a.sent();
                        if (!(producto_pendiente.size === 0 && producto_existente.size === 0)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.alert.create({
                                header: 'El producto no existe',
                                subHeader: '¿Desea registrar el nuevo producto?',
                                message: 'Para registrarlo escriba un nombre y pulse ok, cancelar si no desea registrarlo.',
                                inputs: [{
                                        type: 'text',
                                        name: 'nombre',
                                        value: '',
                                        label: 'Nombre',
                                        placeholder: 'Nombre del nuevo producto',
                                    }],
                                buttons: [{
                                        text: 'Cancelar',
                                        role: 'cancel',
                                        handler: function () {
                                        }
                                    }, {
                                        text: 'Aceptar',
                                        handler: function (res) {
                                            return __awaiter(this, void 0, void 0, function () {
                                                var nuevo, toast_alert;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            nuevo = {
                                                                nombre: res.nombre,
                                                                barcode: barcode.text,
                                                                fecha_alta: new Date().toLocaleString(),
                                                                fecha_pendiente: new Date().toLocaleString()
                                                            };
                                                            if (!(nuevo.nombre !== '')) return [3 /*break*/, 3];
                                                            console.log(nuevo);
                                                            return [4 /*yield*/, self.firestore.collection('producto').add(nuevo)];
                                                        case 1:
                                                            _a.sent();
                                                            return [4 /*yield*/, self.firestore.collection('pendiente').add(nuevo)];
                                                        case 2:
                                                            _a.sent();
                                                            return [3 /*break*/, 6];
                                                        case 3: return [4 /*yield*/, self.toast.create({
                                                                message: 'El nombre del producto no puede estar vacio!',
                                                                position: 'middle',
                                                                duration: 3000,
                                                            })];
                                                        case 4:
                                                            toast_alert = _a.sent();
                                                            return [4 /*yield*/, toast_alert.present()];
                                                        case 5:
                                                            _a.sent();
                                                            _a.label = 6;
                                                        case 6: return [2 /*return*/];
                                                    }
                                                });
                                            });
                                        }
                                    },]
                            })];
                    case 4:
                        alert_registrar = _a.sent();
                        return [4 /*yield*/, alert_registrar.present()];
                    case 5:
                        _a.sent();
                        return [3 /*break*/, 7];
                    case 6:
                        if (producto_pendiente.size === 0 && producto_existente.size === 1) {
                            // Añadimos el producto a la lista de la compra.
                            producto_existente.forEach(function (item) {
                                return __awaiter(this, void 0, void 0, function () {
                                    var nuevo_pendiente, alert_add;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                nuevo_pendiente = item.data();
                                                nuevo_pendiente.fecha_pendiente = new Date().toLocaleString();
                                                return [4 /*yield*/, self.alert.create({
                                                        header: 'Unideades',
                                                        message: '¿Cuantas unidades del producto desea añadir a la cesta?',
                                                        inputs: [{
                                                                name: 'unidades',
                                                                label: 'Unidades',
                                                                type: 'number',
                                                                value: 1,
                                                                min: 1,
                                                                max: 50
                                                            },],
                                                        buttons: [{
                                                                text: 'Aceptar',
                                                                handler: function (res) {
                                                                    return __awaiter(this, void 0, void 0, function () {
                                                                        var existe_pendiente, toast_add;
                                                                        return __generator(this, function (_a) {
                                                                            switch (_a.label) {
                                                                                case 0:
                                                                                    nuevo_pendiente.fecha_pendiente = new Date().toLocaleString();
                                                                                    nuevo_pendiente.unidades = res.unidades;
                                                                                    return [4 /*yield*/, self.firestore.collection('pendiente', function (ref) { return ref.where('nombre', '==', nuevo_pendiente.nombre); }).get().toPromise()];
                                                                                case 1:
                                                                                    existe_pendiente = _a.sent();
                                                                                    if (!(existe_pendiente.size === 0)) return [3 /*break*/, 5];
                                                                                    console.log('Existe nuevo pendiente');
                                                                                    return [4 /*yield*/, self.firestore.collection('pendiente').add(nuevo_pendiente)];
                                                                                case 2:
                                                                                    _a.sent();
                                                                                    return [4 /*yield*/, self.toast.create({
                                                                                            message: 'Producto añadido a la cesta',
                                                                                            position: 'middle',
                                                                                            duration: 3000
                                                                                        })];
                                                                                case 3:
                                                                                    toast_add = _a.sent();
                                                                                    return [4 /*yield*/, toast_add.present()];
                                                                                case 4:
                                                                                    _a.sent();
                                                                                    _a.label = 5;
                                                                                case 5: return [2 /*return*/];
                                                                            }
                                                                        });
                                                                    });
                                                                }
                                                            },
                                                            {
                                                                text: 'Cancelar',
                                                                handler: function () { },
                                                            },],
                                                    })];
                                            case 1:
                                                alert_add = _a.sent();
                                                alert_add.present();
                                                return [2 /*return*/];
                                        }
                                    });
                                });
                            });
                        }
                        else if (producto_pendiente.size === 1 && producto_existente.size === 1) {
                            // Añadimos el producto a la lista de comprados y lo eliminamos de la lista de la compra.
                            producto_pendiente.forEach(function (item) {
                                return __awaiter(this, void 0, void 0, function () {
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0: return [4 /*yield*/, self.firestore.collection('comprado').add(item.data())];
                                            case 1:
                                                _a.sent();
                                                return [4 /*yield*/, self.firestore.doc(item.ref).delete()];
                                            case 2:
                                                _a.sent();
                                                return [2 /*return*/];
                                        }
                                    });
                                });
                            });
                        }
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.producto_comprado = function (producto, slidingItem) {
        return __awaiter(this, void 0, void 0, function () {
            var self, producto_pendiente;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(producto);
                        self = this;
                        return [4 /*yield*/, this.firestore.collection('pendiente', function (ref) { return ref.where('nombre', '==', producto.nombre); }).get().toPromise()];
                    case 1:
                        producto_pendiente = _a.sent();
                        producto_pendiente.forEach(function (item) {
                            return __awaiter(this, void 0, void 0, function () {
                                var comprado;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            comprado = item.data();
                                            comprado.fecha_compra = new Date().toLocaleString();
                                            return [4 /*yield*/, self.firestore.collection('comprado').add(comprado)];
                                        case 1:
                                            _a.sent();
                                            slidingItem.close();
                                            return [4 /*yield*/, self.firestore.doc(item.ref).delete()];
                                        case 2:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Elimina un producto de la lista de pendiente sin añadirlo a comprados.
     * @param producto
     */
    HomePage.prototype.producto_eliminado = function (producto, slidingItem) {
        return __awaiter(this, void 0, void 0, function () {
            var self, producto_pendiente;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(producto);
                        self = this;
                        return [4 /*yield*/, this.firestore.collection('pendiente', function (ref) { return ref.where('nombre', '==', producto.nombre); }).get().toPromise()];
                    case 1:
                        producto_pendiente = _a.sent();
                        producto_pendiente.forEach(function (item) {
                            return __awaiter(this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            slidingItem.close();
                                            return [4 /*yield*/, self.firestore.doc(item.ref).delete()];
                                        case 1:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'page-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_2__["BarcodeScanner"],
            _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_1__["LocalNotifications"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_0__["AngularFirestore"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map