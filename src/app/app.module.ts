import { NgModule } from '@angular/core';
import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation/ngx';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import 'hammerjs';

const firebaseConfig = {
  apiKey: 'AIzaSyDkDTubWmTZ9TsF6d9j1WiA8f8NjnavMJc',
  authDomain: 'compra-ba403.firebaseapp.com',
  databaseURL: 'https://compra-ba403.firebaseio.com',
  projectId: 'compra-ba403',
  storageBucket: 'compra-ba403.appspot.com',
  messagingSenderId: '308404021450'
};

/**
 * Arregla el problema del scroll.
 */
export class BaluHammerConfig extends HammerGestureConfig {
  overrides = {
      pan: {
          direction: 6
      },
      pinch: {
          enable: false
      },
      rotate: {
          enable: false
      }
  };
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
     AppRoutingModule,
     AngularFireModule.initializeApp(firebaseConfig),
     AngularFirestoreModule
    ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    LocalNotifications,
    BackgroundGeolocation,
    Geolocation,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HAMMER_GESTURE_CONFIG, useClass: BaluHammerConfig },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
