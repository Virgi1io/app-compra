import { AngularFirestore } from '@angular/fire/firestore';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation/';
import { AlertController, ToastController } from '@ionic/angular';

const config: BackgroundGeolocationConfig = {
  desiredAccuracy: 10,
  stationaryRadius: 20,
  distanceFilter: 30,
  debug: true, //  enable this hear sounds for background-geolocation life-cycle.
  stopOnTerminate: false, // enable this to clear background location settings when the app terminates
};

@Component({
  selector: 'page-home',
  templateUrl: 'home.page.html',
  styleUrls: ['./home.page.scss', ],
})
export class HomePage implements OnInit {

  public lista_productos_pendientes = [];
  public lista = false;

  constructor(public scanner: BarcodeScanner,
    public notifyer: LocalNotifications,
    public firestore: AngularFirestore,
    public gps: Geolocation,
    public alert: AlertController,
    public  toast: ToastController) {}

  /**
   * Metodo inicial de la lista.
   */
  ngOnInit() {
    this.iniciar_lista();
  }

  /**
   * Inicia la lista de las compras pendientes
   */
  iniciar_lista() {
    this.lista_productos_pendientes = [];
    this.firestore.collection('pendiente').valueChanges().subscribe(res => {
      this.lista = res.length === 0;
      this.lista_productos_pendientes = res;
    });
  }

  filtrar(event) {
    const value =  event.target.value;
    if (value && value.trim() != '') {
        this.lista_productos_pendientes = this.lista_productos_pendientes.filter((item) => {
          return (item.nombre.toLowerCase().indexOf(value.toLowerCase()) > -1);
        });
      } else {
        this.iniciar_lista();
      }
  }


  /**
   * Escanea un código con el siguiente comportamiento:
   * Si el producto no esta registrado y no esta en la lista de la compra, lo registra y lo añade a la compra.
   * Si el producto no esta en la lista pero esta registrado, se añade a la lista de la compra.
   * Si el producto esta registrado y en la lista de la compra pasa a comprados y se elimina de la lista de la compra.
   */
  async scan_barcode() {
    const self = this;

    const barcode = await this.scanner.scan({showTorchButton: true});

    const producto_pendiente = await this.firestore.collection
                                          ('pendiente', (ref) => ref.where('barcode', '==', barcode.text)).get().toPromise();
    const producto_existente = await this.firestore.collection
                                          ('producto', (ref) => ref.where('barcode', '==', barcode.text)).get().toPromise();

    if (producto_pendiente.size === 0 && producto_existente.size === 0) {
      // Existe pero no esta en la compra.(Registrar)
      const alert_registrar = await this.alert.create({
        header: 'El producto no existe',
        subHeader: '¿Desea registrar el nuevo producto?',
        message: 'Para registrarlo escriba un nombre y pulse ok, cancelar si no desea registrarlo.',
        inputs: [ {
          type: 'text',
          name: 'nombre',
          value: '',
          label: 'Nombre',
          placeholder: 'Nombre del nuevo producto',
        }],
        buttons: [ {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        }, {
          text: 'Aceptar',
          handler: async function(res) {

            // Creamos un nuevo producto.
            const nuevo = {
              nombre: res.nombre,
              barcode: barcode.text,
              fecha_alta: new Date().toLocaleString(),
              fecha_pendiente: new Date().toLocaleString() };

            if (nuevo.nombre !== '') {
              console.log(nuevo);

              await self.firestore.collection('producto').add(nuevo);

              await self.firestore.collection('pendiente').add(nuevo);

            } else {
              const toast_alert = await self.toast.create({
                message: 'El nombre del producto no puede estar vacio!',
                position: 'middle',
                duration: 3000, });
                    await toast_alert.present();
                  }}}, ]});

        await alert_registrar.present();

    } else if (producto_pendiente.size === 0 && producto_existente.size === 1) {

      // Añadimos el producto a la lista de la compra.
      producto_existente.forEach(async function(item) {
        const nuevo_pendiente = item.data();
        nuevo_pendiente.fecha_pendiente = new Date().toLocaleString();

        const alert_add = await self.alert.create({
          header: 'Unideades',
          message: '¿Cuantas unidades del producto desea añadir a la cesta?',
          inputs: [{
            name: 'unidades',
            label: 'Unidades',
            type: 'number',
            value: 1,
            min: 1,
            max: 50
          }, ],
          buttons: [{
            text: 'Aceptar',
            handler: async function(res) {
              nuevo_pendiente.fecha_pendiente = new Date().toLocaleString();
              nuevo_pendiente.unidades = res.unidades;

              const existe_pendiente = await self.firestore.collection
              ('pendiente', (ref) => ref.where('nombre', '==', nuevo_pendiente.nombre)).get().toPromise();

              if (existe_pendiente.size === 0) {
                console.log('Existe nuevo pendiente');
                await self.firestore.collection('pendiente').add(nuevo_pendiente);
                const toast_add = await self.toast.create({
                  message: 'Producto añadido a la cesta',
                  position: 'middle',
                  duration: 3000
                });
                await toast_add.present();
              }
            }
          },
          {
            text: 'Cancelar',
            handler: () => {},
          }, ],
        });
        alert_add.present();

        });
    } else if (producto_pendiente.size === 1 && producto_existente.size === 1) {

      // Añadimos el producto a la lista de comprados y lo eliminamos de la lista de la compra.
      producto_pendiente.forEach(async function(item) {
        await self.firestore.collection('comprado').add(item.data());
        await self.firestore.doc(item.ref).delete();
      }); }}

  async producto_comprado(producto, slidingItem) {
    console.log(producto);
    const self = this;
    const producto_pendiente = await this.firestore.collection
                                    ('pendiente', (ref) => ref.where('nombre', '==', producto.nombre)).get().toPromise();
    producto_pendiente.forEach(async function(item) {
      let comprado = item.data();
      comprado.fecha_compra = new Date().toLocaleString();
      await self.firestore.collection('comprado').add(comprado);
      slidingItem.close();
      await self.firestore.doc(item.ref).delete();
    });
  }

  /**
   * Elimina un producto de la lista de pendiente sin añadirlo a comprados.
   * @param producto
   */
  async producto_eliminado(producto, slidingItem) {
    console.log(producto);
    const self = this;
    const producto_pendiente = await this.firestore.collection
                                    ('pendiente', (ref) => ref.where('nombre', '==', producto.nombre)).get().toPromise();
    producto_pendiente.forEach(async function(item) {
      slidingItem.close();
      await self.firestore.doc(item.ref).delete();
    });
  }

}

