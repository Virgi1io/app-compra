import { AngularFirestore } from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productos-comprados',
  templateUrl: './productos-comprados.page.html',
  styleUrls: ['./productos-comprados.page.scss'],
})
export class ProductosCompradosPage implements OnInit {

  public lista_comprados = [];

  constructor(private firestore: AngularFirestore) {}

  ngOnInit() {
    this.iniciar_lista();
  }

  iniciar_lista() {
    this.lista_comprados = [];
    this.firestore.collection('comprado').valueChanges().subscribe((data) => {
      console.log(data);
      this.lista_comprados = data;
    });
  }

  filtrar(event) {
    const value =  event.target.value;
    if (value && value.trim() != '') {
        this.lista_comprados = this.lista_comprados.filter((item) => {
          return (item.nombre.toLowerCase().indexOf(value.toLowerCase()) > -1);
        });
      } else {
        this.iniciar_lista();
      }
  }
}
