import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductosCompradosPage } from './productos-comprados.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosCompradosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductosCompradosPage]
})
export class ProductosCompradosPageModule {}
