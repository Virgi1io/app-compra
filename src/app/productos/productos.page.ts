import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AngularFirestore } from '@angular/fire/firestore';
import { Platform, AlertController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { firestore } from 'firebase';
import { SelectorListContext } from '@angular/compiler';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.page.html',
  styleUrls: ['./productos.page.scss'],
})
export class ProductosPage implements OnInit {

  public lista_productos = [];
  public seleccion_multiple = false;

  constructor(private router: Router,
    private platform: Platform,
    private firestore: AngularFirestore,
    private alertctrl: AlertController,
    private toastctrl: ToastController,
    private scanner: BarcodeScanner) {
      this.platform.backButton.subscribe(() => {
        this.router.navigateByUrl('/productos');
        this.seleccion_multiple = false;
        this.lista_productos.forEach((producto) => {
          producto.seleccionado = false;
        });
      });
  }

  ngOnInit() {
    this.iniciar_lista();
  }

  iniciar_lista() {
    this.lista_productos = [];
    this.firestore.collection('producto', (ref) => ref.orderBy('nombre')).valueChanges().subscribe((data) => {
      console.log(data);
      data.forEach((item: any) => {
        item.seleccionado = false;
      });
      this.lista_productos = data;
    });
  }

  filtrar(event) {
    const value =  event.target.value;
    if (value && value.trim() != '') {
        this.lista_productos = this.lista_productos.filter((item) => {
          return (item.nombre.toLowerCase().indexOf(value.toLowerCase()) > -1);
        });
      } else {
        this.iniciar_lista();
      }
  }

  async nuevo(barcode= '') {
    const self = this;
    let nuevo_producto;
    const alert_registrar = await this.alertctrl.create({
      header: 'Añadir nuevo producto:',
      subHeader: '¿Desea registrar el nuevo producto?',
      message: 'Para registrarlo escriba un nombre y pulse ok, cancelar si no desea registrarlo.',
      inputs: [ {
        type: 'text',
        name: 'nombre',
        value: '',
        label: 'Nombre',
        placeholder: 'Nombre del nuevo producto',
      }],
      buttons: [ {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
        }
      }, {
        text: 'Aceptar',
        handler: async function(res) {

          // Creamos un nuevo producto.
          nuevo_producto = {
            nombre: res.nombre,
            barcode: barcode,
            fecha_alta: new Date().toLocaleString()
          };

          console.log(nuevo_producto);
          if (nuevo_producto.nombre !== '') {

            self.firestore.collection('producto').add(nuevo_producto).then(add_res => {
              console.log('Confirm Okay', add_res);
            }).catch(err => console.error(err));

          } else {
            const toast_alert = await self.toastctrl.create({
              message: 'El nombre del producto no puede estar vacio!',
              position: 'middle',
              duration: 3000, });
                  await toast_alert.present();
                }}}, ]});

      await alert_registrar.present();
  }

  async nuevo_con_codigo() {
    const self = this;
    const barcode = await this.scanner.scan({showTorchButton: true});
    console.log('Leido codigo', barcode.text);
    const producto_existente = await this.firestore.collection
                                          ('producto', (ref) => ref.where('barcode', '==', barcode.text)).get().toPromise();
    console.log('productos encontrados :', producto_existente.size);
    if (producto_existente.size === 0) {
      // El producto no existe, vamos a registrarlo.
      this.nuevo(barcode.text);
      const toast = await self.toastctrl.create({
        message: 'Producto registrado!',
        position: 'middle',
        duration: 3000
      });
      await toast.present();

    } else if (producto_existente.size === 1) {

      // El producto existe vamos a añadirlo a la lista de la compra pendiente.
      producto_existente.forEach(async function(item) {
        self.pasar_a_pendiente(item.data());
      });
  }
}

  async eliminar_registrado(producto, slidingItem) {
    console.log(producto);
    const self = this;
    const alert_registrar = await this.alertctrl.create({
      header: 'Eliminar el producto ' + producto.nombre,
      subHeader: '¿Desea eliminar el producto?',
      message: 'Para eliminarlo pulse aceptar, cancelar si no desea registrarlo.',
      buttons: [ {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
        }
      }, {
        text: 'Aceptar',
        handler: async function() {
          const producto_a_borrar = await self.firestore.collection
          ('producto', (ref) => ref.where('nombre', '==', producto.nombre)).get().toPromise();
          producto_a_borrar.forEach((item) => {
            slidingItem.close();
            item.ref.delete();
          });
        }}, ]});
      await alert_registrar.present();
  }

  async pasar_a_pendiente(producto, slidingItem= {}, multiple= false) {
    const self = this;
    const alerta = await this.alertctrl.create({
      header: 'Unidades:',
      subHeader: '¿Cuantas unidades del producto ' + producto.nombre + ' desea añadir a la cesta ?',
      message: 'Seleccione con el teclado cuantas unidades del producto quiere añadir a la cesta.',
      inputs: [{
        name: 'unidades',
        label: 'Unidades',
        type: 'number',
        value: 1,
        min: 1,
        max: 50
      }, ],
      buttons: [{
        text: 'Aceptar',
        handler: async function(res) {
          const nuevo_pendiente = producto;
          nuevo_pendiente.fecha_pendiente = new Date().toLocaleString();
          nuevo_pendiente.unidades = res.unidades;

          const existe_pendiente = await self.firestore.collection
          ('pendiente', (ref) => ref.where('nombre', '==', nuevo_pendiente.nombre)).get().toPromise();

          console.log(existe_pendiente);

          if (existe_pendiente.size === 0) {
            console.log('Existe nuevo pendiente');
            await self.firestore.collection('pendiente').add(nuevo_pendiente);
            const toast = await self.toastctrl.create({
              message: 'Producto añadido a la cesta',
              position: 'middle',
              duration: 3000
            });
            await toast.present();
          }
        }
      },
      {
        text: 'Cancelar',
        handler: () => {},
      }, ],
    });
    if ((!this.seleccion_multiple) || (this.seleccion_multiple && multiple)) {
      await alerta.present();
    }
  }

  activar_seleccion_multiple(producto) {
    this.seleccion_multiple = true;
    producto.seleccionado = true;
  }

  pasar_compra() {
    console.log("seleccion multiple", this.lista_productos);
    this.lista_productos.forEach((producto) => {
      if (producto.seleccionado) {
        this.pasar_a_pendiente(producto, {}, true);
      }
    });
  }


}
